/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.aofcosta.tools.note2self;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class NotesActivity extends AppCompatActivity {

    private MenuItem sendItem;
    private MenuItem progressBarItem;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_notes_activity);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeSettings();
        openKeyboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.progressBarItem = menu.findItem(R.id.progressBar);
        this.sendItem = menu.findItem(R.id.send);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();

        switch (itemId) {
            case R.id.send:
                return sendNote();
            case R.id.settings:
                return goToSettings();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean sendNote() {
        final AppPreferences serverDetails = AppPreferences.fromSharedPrefs(
                PreferenceManager.getDefaultSharedPreferences(this));

        if (serverDetails == null) {
            Toast.makeText(this,
                    getString(R.string.settings_please_fill_in),
                    Toast.LENGTH_SHORT).show();
            goToSettings();
            return true;
        }

        final NotesFragment notesFragment
                = (NotesFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_notes);

        if (notesFragment != null) {
            final NotesFragment.OnStatusChangeListener statusListener
                    = new NotesFragment.OnStatusChangeListener() {
                @Override
                public void onStatusChange(int status) {
                    if (status == NotesFragment.STATUS_CONNECTING) {
                        showProgressBar(true);
                    } else if (status == NotesFragment.STATUS_DONE) {
                        showProgressBar(false);

                        if (notesFragment.hasIncomingData()) {
                            NotesActivity.this.finish();
                        }
                    }
                }
            };

            notesFragment.sendTo(serverDetails, statusListener);
        }

        return true;
    }

    private boolean goToSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
        return true;
    }

    private void initializeSettings() {
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }

    private void openKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void showProgressBar(boolean visible) {
        this.progressBarItem.setVisible(visible);
        this.sendItem.setVisible(!visible);
    }
}
