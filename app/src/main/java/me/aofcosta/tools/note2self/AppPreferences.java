/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.aofcosta.tools.note2self;

import android.content.SharedPreferences;

public class AppPreferences {

    private final String emailAddress;
    private final String host;
    private final int port;
    private final boolean ssl;
    private final boolean starttls;
    private final String user;
    private final String password;

    public static AppPreferences fromSharedPrefs(final SharedPreferences preferences) {
        final String emailAddress = preferences.getString(SettingsFragment.KEY_EMAIL_ADDRESS, "");
        final String protocol = preferences.getString(SettingsFragment.KEY_IMAP_PROTOCOL, "");
        final String host = preferences.getString(SettingsFragment.KEY_IMAP_HOST, "");
        final String portStr = preferences.getString(SettingsFragment.KEY_IMAP_PORT, "");
        final String user = preferences.getString(SettingsFragment.KEY_IMAP_USER, "");
        final String password = preferences.getString(SettingsFragment.KEY_IMAP_PASSWORD, "");

        if (emailAddress.isEmpty() || protocol.isEmpty() || host.isEmpty() || portStr.isEmpty()) {
            return null;
        }

        final int port;

        try {
            port = Integer.parseInt(portStr, 10);
        } catch (NumberFormatException ex) {
            return null;
        }

        return new AppPreferences(
                emailAddress,
                host, port,
                "ssl".equals(protocol),
                "starttls".equals(protocol),
                user, password);
    }

    private AppPreferences(
            final String emailAddress,
            final String host,
            final int port,
            final boolean ssl,
            final boolean starttls,
            final String user,
            final String password) {
        this.emailAddress = emailAddress;
        this.host = host;
        this.port = port;
        this.ssl = ssl;
        this.starttls = starttls;
        this.user = user;
        this.password = password;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public boolean isSSL() {
        return ssl;
    }

    public boolean isStartTLS() {
        return starttls;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
