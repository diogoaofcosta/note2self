/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.aofcosta.tools.note2self;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.widget.EditText;

import com.takisoft.fix.support.v7.preference.EditTextPreference;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

public class SettingsFragment extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public final static String KEY_EMAIL_ADDRESS = "email_address";
    public final static String KEY_IMAP_PROTOCOL = "imap_protocol";
    public final static String KEY_IMAP_HOST = "imap_host";
    public final static String KEY_IMAP_PORT = "imap_port";
    public static final String KEY_IMAP_USER = "imap_user";
    public static final String KEY_IMAP_PASSWORD = "imap_password";

    @Override
    public void onCreatePreferencesFix(Bundle bundle, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
        refreshAllPrefSummaries();
    }

    @Override
    public void onResume() {
        super.onResume();

        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        refreshPrefSummary(key);
    }

    private void refreshAllPrefSummaries() {
        for (final String key : getPreferenceScreen().getSharedPreferences().getAll().keySet()) {
            refreshPrefSummary(key);
        }
    }

    private void refreshPrefSummary(final String key) {
        final Preference pref = findPreference(key);

        if (pref == null) {
            return;
        }

        if (pref instanceof ListPreference) {
            final ListPreference listPref = (ListPreference) pref;

            if (listPref.getEntry() != null) {
                pref.setSummary(listPref.getEntry());
            }
        } else if (pref instanceof EditTextPreference) {
            final EditText field = ((EditTextPreference) pref).getEditText();
            final CharSequence val = field.getTransformationMethod().getTransformation(
                    getPreferenceScreen().getSharedPreferences().getString(key, ""),
                    field);

            pref.setSummary(val);
        }
    }
}
