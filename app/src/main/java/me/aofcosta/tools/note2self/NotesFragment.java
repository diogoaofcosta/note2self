/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.aofcosta.tools.note2self;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class NotesFragment extends Fragment {

    public static int STATUS_CONNECTING = 1;
    public static int STATUS_OPENING_FOLDER = 2;
    public static int STATUS_STORING_MESSAGE = 3;
    public static int STATUS_SUCCESS = 4;
    public static int STATUS_ERROR = 5;
    public static int STATUS_DONE = 6;

    private boolean incomingData;

    @Nullable
    @Override
    public View onCreateView(
            @NonNull final LayoutInflater inflater,
            @Nullable final ViewGroup container,
            @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_notes, container, false);
        this.incomingData = false;
        initializeNotes(view);
        return view;
    }

    private void initializeNotes(@NonNull final View view) {
        final EditText notes = view.findViewById(R.id.notes);
        final String incomingData = getIncomingData();

        if (incomingData != null) {
            this.incomingData = true;
            notes.setText(incomingData);
        }

        notes.requestFocus();
    }

    public boolean hasIncomingData() {
        return this.incomingData;
    }

    private String getIncomingData() {
        if (getActivity() == null) {
            return null;
        }

        final Intent intent = getActivity().getIntent();
        final String action = intent.getAction();

        if (!Intent.ACTION_SEND.equals(action)) {
            return null;
        }

        final String type = intent.getType();

        if (type == null) {
            return null;
        }

        if ("text/plain".equals(type)) {
            final String title = intent.getStringExtra(Intent.EXTRA_SUBJECT);
            final String content = intent.getStringExtra(Intent.EXTRA_TEXT);

            if (title == null) {
                return content;
            } else if (content == null) {
                return title;
            }

            return title + "\n" + content;
        }

        return null;
    }

    public void sendTo(AppPreferences preferences, final OnStatusChangeListener listener) {
        final View view = getView();

        if (view == null) {
            return;
        }

        final EditText notesView = view.findViewById(R.id.notes);
        final String notes = notesView.getText().toString();

        if (notes.isEmpty()) {
            Toast.makeText(getContext(),
                    getString(R.string.nothing_to_send), Toast.LENGTH_SHORT).show();
            return;
        }

        final OnStatusChangeListener changeListener = new OnStatusChangeListener() {
            @Override
            public void onStatusChange(int status) {
                if (status == STATUS_SUCCESS) {
                    notesView.setText("");

                    final String msg = getString(R.string.success_note_saved);
                    Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                }

                if (listener != null) {
                    listener.onStatusChange(status);
                }
            }
        };

        final StoreEmail.OnExceptionListener onException = new StoreEmail.OnExceptionListener() {
            @Override
            public void onException(Throwable ex) {
                final String msg = getString(R.string.error_saving_note, ex.getLocalizedMessage());
                Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
            }
        };

        new StoreEmail(preferences, notes, onException, changeListener).execute();
    }


    public interface OnStatusChangeListener {

        void onStatusChange(int status);
    }

    public static class StoreEmail extends AsyncTask<Void, Integer, Throwable> {

        public interface OnExceptionListener {
            void onException(Throwable ex);
        }

        private final AppPreferences preferences;
        private final String notes;
        private final OnExceptionListener onExceptionListener;
        private final OnStatusChangeListener statusChangeListener;

        StoreEmail(AppPreferences preferences, String notes,
                   OnExceptionListener onExceptionListener,
                   OnStatusChangeListener statusChangeListener) {
            this.preferences = preferences;
            this.notes = notes;
            this.onExceptionListener = onExceptionListener;
            this.statusChangeListener = statusChangeListener;
        }

        @Override
        protected Throwable doInBackground(Void... params) {
            final Properties props = new Properties();

            props.put("mail.store.protocol", "imap");
            props.put("mail.imap.connectiontimeout", "5000");
            props.put("mail.imap.timeout", "10000");
            props.put("mail.imap.host", preferences.getHost());
            props.put("mail.imap.port", "" + preferences.getPort());
            props.put("mail.imap.user", preferences.getUser());
            props.put("mail.imap.password", "" + preferences.getPassword());
            props.put("mail.debug.auth", "true");

            if (preferences.isStartTLS()) {
                props.put("mail.imap.starttls.enable", "true");
                props.put("mail.imap.starttls.required", "true");
            } else if (preferences.isSSL()) {
                props.put("mail.imap.ssl.enable", "true");
            }

            if (preferences.isSSL() || preferences.isStartTLS()) {
                props.put("mail.imap.ssl.trust", "*");
                props.put("mail.imap.ssl.checkserveridentity", "false");
            }

            final Session session = Session.getInstance(props, null);
            session.setDebug(true);
            session.setDebugOut(System.err);
            Store store;
            Folder inbox = null;

            try {
                store = session.getStore();
            } catch (NoSuchProviderException e) {
                Log.e(StoreEmail.class.getName(), "No IMAP provider.", e);
                return e;
            }

            try {
                store.connect(null, null, preferences.getPassword());
            } catch (MessagingException e) {
                Log.e(StoreEmail.class.getName(), "Couldn't connect to preferences", e);
                return e;
            }

            try {
                setStatus(STATUS_OPENING_FOLDER);
                inbox = store.getFolder("INBOX");

                setStatus(STATUS_STORING_MESSAGE);
                final MimeMessage msg = new MimeMessage(session);
                msg.addHeader("Content-Type", "text/plain; charset=UTF-8");
                msg.addHeader("format", "flowed");
                msg.addHeader("Content-Transfer-Encoding", "8bit");
                msg.setFrom(new InternetAddress("Note2self <" + preferences.getEmailAddress() + ">"));
                msg.addRecipients(Message.RecipientType.TO, preferences.getEmailAddress());
                msg.setSubject(getSubject(notes), "UTF-8");
                msg.setSentDate(new Date());
                msg.setText(notes, "UTF-8");

                inbox.appendMessages(new Message[]{msg});
                setStatus(STATUS_SUCCESS);
            } catch (MessagingException e) {
                setStatus(STATUS_ERROR);
                Log.e(StoreEmail.class.getName(), "Couldn't store message in preferences", e);
                return e;
            } finally {
                if (inbox != null && inbox.isOpen()) {
                    try {
                        inbox.close(false);
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                }

                if (store.isConnected()) {
                    try {
                        store.close();
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            if (statusChangeListener != null) {
                statusChangeListener.onStatusChange(STATUS_CONNECTING);
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (statusChangeListener != null) {
                statusChangeListener.onStatusChange(values[0]);
            }
        }

        @Override
        protected void onPostExecute(Throwable o) {
            if (o != null && onExceptionListener != null) {
                onExceptionListener.onException(o);
            }
            if (statusChangeListener != null) {
                statusChangeListener.onStatusChange(STATUS_DONE);
            }
        }

        private String getSubject(String notes) {
            final int newLine = notes.indexOf('\n');

            return newLine == -1 ? notes : notes.substring(0, newLine);
        }

        private void setStatus(int status) {
            publishProgress(status);
        }
    }
}
