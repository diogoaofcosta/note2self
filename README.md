# Note2self

## Description

This simple application allows you to send short notes directly to your email account.
By storing these notes in your email account, you can access them anywhere, with any normal email client.

## Use cases

### Capture your thoughts

One of the most important aspects of the GTD methodology is taking note of all your ideas and thoughts.
Add the Note2self application to your launcher, as a favorite, and you are one click away from a place to jot down your next brilliant idea.
Another click and it is delivered to your inbox, where you can pick it up later and process it.

### Read-later

You can use Android's "Share with..." built-in functionality to send data from other applications into your inbox:

1. Open a webpage in your browser, select "Share" and click on "Note2self"
2. The application will open and the notes field will be populated with the page's title and link
3. Press the "Send" button to send it and close the app.

## Screenshots

Check the [screenshots](screenshots.md) page.

## Setup

To start using the application you must first setup your email account. The application uses the IMAP protocol to store notes in a folder of your choosing.

1. Open the preferences page by clicking on the drop-down icon on the upper-right corner and selecting "Settings"
2. Click on the "Protocol", "Server", "Username", "Password" options and fill them in with the details of your IMAP server
	1. TODO: Add example options for Gmail
	2. TODO: Add example options for Office 365
3. After filling the configurations above, click on the "Destination Folder" option. This will show you a list of the folder available in your email account, from which you must choose one.
	1. In case of a connection error, fix the configurations from the previous step and try again
4. Press the back button to exit the preferences page

Congratulations! You are now ready to start adding notes!

## Building

Use the provided Gradle wrapper scripts to build the APK:

	./gradlew clean build

The APK will be located in the "app/build/outputs" folder.

